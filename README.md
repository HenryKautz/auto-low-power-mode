# Auto Low Power Mode

This is a Mac OS X shortcut that automatically puts the machine into Low Power Mode when the battery level falls below 50%, and automatically 
turns off Low Power Mode when the battery level rises about 50%.  The threshold can be changed when the shortcut is launched.

## Installation

Double-click the Auto Low Power Mode icon.  Shortcuts will ask if you
want to add it to your shortcuts.  Click yes.  Open it in Shortcuts.  Under the File menu, select Add to Dock.  The shortcut will be installed as an application in /Users/YOUR\_NAME/Applications.  Open Settings, Login Items, the plus sign, and navigate to the Auto Low Power Mode application.

The first time you run the shortcut, it will ask permission to run a shell script.  When the shortcut starts up at login, it will ask you for the battery level at which to trigger Low Power Mode.  After about 27 hours, it will ask you again for the battery level at which to trigger Low Power Mode.